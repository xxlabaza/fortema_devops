#!/usr/bin/env bash

set -e


TIMEFORMAT='[INFO]: Elapsed real time %E seconds'
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PREFIX_IMAGE_NAME="fortema"


build_image() {
    FULL_IMAGE_NAME="$PREFIX_IMAGE_NAME/$1"
    echo "[INFO]: Building $FULL_IMAGE_NAME"
    time docker build --force-rm=true --tag $FULL_IMAGE_NAME $CURRENT_DIR/$1
    echo "[INFO]: $FULL_IMAGE_NAME has been builded"
}

if [ "$#" -ne 1 ];
then
    shopt -u dotglob
    find $CURRENT_DIR -type f -maxdepth 2 -name 'Dockerfile' | rev | cut -d '/' -f 2 | rev | while read IMAGE_NAME;
    do
        if [ -z $(docker images -q $PREFIX_IMAGE_NAME/$IMAGE_NAME) ];
        then
            build_image $IMAGE_NAME
        fi
    done
else
    build_image ${1%/}
fi
